package psybergate.grad2019.javafnds.jee.ce1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import demo.interfaces.dates.standard.Date;
import demo.interfaces.dates.standard.DateFactory;
import demo.interfaces.dates.standard.InvalidDateException;

/**
 * 
 * Red -> Green -> Refactor
 *
 * Red: Tests must fail first (all) Green: Make the tests pass (as
 * quickly/simply as possible) Refactor:
 * 
 * @author countach
 *
 */
public class MyDateTest {

	private Date date;
	private DateFactory factory;

	public MyDateTest(String chosenDateFactory)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvalidDateException {

		factory = (DateFactory) Class.forName(chosenDateFactory).newInstance();
		date = factory.createDate(1998, 5, 15);
	}

	@Test(expected = InvalidDateException.class)
	public void testNegativeDay() throws InvalidDateException {

		factory.createDate(2015, 5, -3);
	}

	@Test(expected = InvalidDateException.class)
	public void testNegativeMonth() throws InvalidDateException {

		factory.createDate(2015, -5, 15);
	}

	@Test
	public void testNegativeYear() throws InvalidDateException {

		factory.createDate(-2025, 5, 19);
	}

	@Test(expected = InvalidDateException.class)
	public void testZeroDateCreation() throws InvalidDateException {

		factory.createDate(0, 0, 0);
	}

	@Test(expected = InvalidDateException.class)
	public void testInvalidDates() {

		try {
			factory.createDate(2002, 78, 88);
			factory.createDate(2, 741, 55);
			fail("Expecting a");
		} catch (InvalidDateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testValidDates() throws InvalidDateException {

		Date d1 = createDate(2010, 5, 15);
		Date d2 = factory.createDate(2010, 3, 14);
		Date d3 = factory.createDate(1998, 1, 9);
		Date d4 = factory.createDate(2018, 8, 25);

		assertEquals(17, d1.getDay());
		assertEquals(3, d2.getMonth());
		assertEquals(1998, d3.getYear());
		assertEquals(25, d4.getDay());
	}

	private void createInvalidDate(int year, int months, int days) {

	}

	private void createValidDate(int year, int months, int days) {

	}

	public Date createDate(int year, int month, int day) throws InvalidDateException {
		return factory.createDate(year, month, day);
	}

	@Test
	public void testAddDays() throws InvalidDateException {

		date = factory.createDate(1970, 1, 1);
		Date d1 = date.addDays(2);
		Date d2 = date.addDays(5);
		Date d3 = date.addDays(9);
		Date d4 = date.addDays(10);

		assertEquals(3, d1.getDay());
		assertEquals(1, d2.getMonth());
		assertEquals(1970, d3.getYear());
		assertEquals(11, d4.getDay());
	}
}
