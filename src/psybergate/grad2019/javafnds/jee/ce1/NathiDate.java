package psybergate.grad2019.javafnds.jee.ce1;

import demo.interfaces.dates.standard.Date;

public class NathiDate implements Date {

	private int year;
	private int month;
	private int day;
	
	public NathiDate() {
		
	}
	
	public NathiDate(int year, int month, int day) { 

		this.year = year;
		this.month = month;
		this.day = day;
	}

	@Override
	public int getYear() {
		return year;
	}

	@Override
	public int getMonth() {
		return month;
	}

	@Override
	public int getDay() {
		return day;
	}

	@Override
	public boolean isLeapYear() {
		
		if(year % 100 == 0) {
			if(year % 400 == 0) {
				return true;
			}
		}
		
		if(year % 4 == 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public Date addDays(int numDays) {
		
		int newDay = day + numDays;
		int newMonth = month;
		int newYear = year;
		
		int currentMonthsMaxDays = NathiDateFactory.months.get(month);
		
		if(newDay > currentMonthsMaxDays) {
			int diff = newDay - currentMonthsMaxDays;
			newDay = diff;
			int monthOverflow = diff/30; 
			newMonth = month + ((Integer) monthOverflow);
			
			//Incomplete implementation ...
		}
		
		return new NathiDate(newYear, newMonth, newDay);
	}
	
	

	@Override
	public String toString() {
		return year + ", " + month + ", " + day;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		
		NathiDate other = (NathiDate) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	
	
}
