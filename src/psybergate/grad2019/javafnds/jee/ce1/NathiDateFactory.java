package psybergate.grad2019.javafnds.jee.ce1;

import java.util.HashMap;

import demo.interfaces.dates.standard.Date;
import demo.interfaces.dates.standard.DateFactory;
import demo.interfaces.dates.standard.InvalidDateException;

public class NathiDateFactory implements DateFactory {

	public static HashMap<Integer, Integer> months;
	
	static {
		
		months = new HashMap<>();
		months.put(1, 31);
		months.put(2, 38);
		months.put(3, 31);
		months.put(4, 30);
		months.put(5, 31);
		months.put(6, 30); 
		months.put(7, 31);
		months.put(8, 31);
		months.put(9, 30);
		months.put(10, 31);
		months.put(11, 30);
		months.put(12, 31);
	}
	
	public NathiDateFactory() {

	}
	
	@Override
	public Date createDate(int year, int month, int day) throws InvalidDateException {

		if (!isValidDay(day)) {
			throw new InvalidDateException(1, "Invalid day");
		}

		if (!isValidMonth(month)) {
			throw new InvalidDateException(2, "Invalid month");
		}

		if (!isValidYear(year)) {
			throw new InvalidDateException(3, "Invalid year");
		}

		return new NathiDate(year, month, day);
	}
	
	private boolean isValidDay(int day) {
		return (day >= 1 && day <= 31);
	}

	private boolean isValidMonth(int month) {
		return (month >= 1 && month <= 12);
	}

	private boolean isValidYear(int year) {
		return true;
	}
}
