package psybergate.grad2019.javafnds.jee.ce1;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Initiator {
	
	public static String loadImplementation() {
		
		String fileAsString = "";
		
		try {
			
			InputStream is = new FileInputStream("/home/countach/eclipse-workspace/JEE/src/resources/properties.txt");
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			        
			String line = buf.readLine();
			StringBuilder sb = new StringBuilder();
			        
			while(line != null){
			   sb.append(line).append("\n");
			   line = buf.readLine();
			}
			        
			fileAsString = sb.toString();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return fileAsString;
	}

	public static void main(String[] args) throws Exception {
		
		String file = loadImplementation().trim();
		System.out.println("File loaded: " + file);
		MyDateTest tester = new MyDateTest(file);
		
//		tester.testZeroDateCreation();
		tester.testNegativeYear();
		//tester.testNegativeMonth();
		//tester.testNegativeDay();
		tester.testValidDates();
		//tester.testInvalidDates();
		
		System.out.println("\nAll tests passed!");
	}
}
 