package psybergate.grad2019.javafnds.jee.ce3.standards;

public interface CommandResponse {

	Object getResponseData();
}
