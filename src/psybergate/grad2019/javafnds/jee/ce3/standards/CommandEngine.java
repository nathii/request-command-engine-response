package psybergate.grad2019.javafnds.jee.ce3.standards;

public interface CommandEngine {

	CommandResponse execute();
}
