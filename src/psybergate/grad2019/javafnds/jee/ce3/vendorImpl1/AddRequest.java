package psybergate.grad2019.javafnds.jee.ce3.vendorImpl1;

import java.util.List;

import psybergate.grad2019.javafnds.jee.ce3.standards.CommandRequest;

public class AddRequest implements CommandRequest {

	private List<Integer> operands;
	
	public AddRequest(List<Integer> ops) {
		operands = ops;
	}

	public List<Integer> getOperands() {
		return operands;
	}
	
	public Integer processRequest() {
		
		Integer sum = 0;
		for (Integer integer : operands) {
			sum += integer;
		}
		
		return sum;
	}
}
