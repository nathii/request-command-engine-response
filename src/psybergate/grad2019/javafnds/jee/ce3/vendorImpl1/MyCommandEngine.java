package psybergate.grad2019.javafnds.jee.ce3.vendorImpl1;

import psybergate.grad2019.javafnds.jee.ce3.standards.Command;
import psybergate.grad2019.javafnds.jee.ce3.standards.CommandEngine;
import psybergate.grad2019.javafnds.jee.ce3.standards.CommandResponse;

public class MyCommandEngine implements CommandEngine {

	private Command command;	//Command currently executing
	
	public MyCommandEngine(Command command) {
		this.command = command;
	}

	@Override
	public CommandResponse execute() {
		return command.execute();
	}
}
