package psybergate.grad2019.javafnds.jee.ce3.vendorImpl1;

import java.math.BigInteger;

import psybergate.grad2019.javafnds.jee.ce3.standards.CommandRequest;

public class FactorialRequest implements CommandRequest {

	private final int N;
	
	public FactorialRequest(int n) {
		N = n;
	}

	@Override
	public BigInteger processRequest() {
		
		BigInteger result = new BigInteger("" + 0);
		
		int start = N;
		
		if(start == 0) {
			return new BigInteger("" + 1);
		}
		else {
			
			while(start >= 1) {
				result = result.add(new BigInteger(Integer.toString(start)));
				start--;
			}
		}
		
		return result;
	}
}
