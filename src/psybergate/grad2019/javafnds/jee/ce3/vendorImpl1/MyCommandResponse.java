package psybergate.grad2019.javafnds.jee.ce3.vendorImpl1;

import psybergate.grad2019.javafnds.jee.ce3.standards.CommandResponse;

public class MyCommandResponse implements CommandResponse {

	private Object responseData;

	public MyCommandResponse(Object responseData) {

		this.responseData = responseData;
	}
	
	public Object getResponseData() {
		return responseData;
	}
}
