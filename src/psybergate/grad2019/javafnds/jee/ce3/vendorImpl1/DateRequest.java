package psybergate.grad2019.javafnds.jee.ce3.vendorImpl1;

import java.time.LocalDate;

import psybergate.grad2019.javafnds.jee.ce3.standards.CommandRequest;

public class DateRequest implements CommandRequest {

	@Override
	public LocalDate processRequest() {
		return LocalDate.now();
	}
}
