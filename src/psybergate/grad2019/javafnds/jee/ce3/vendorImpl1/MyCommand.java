package psybergate.grad2019.javafnds.jee.ce3.vendorImpl1;

import psybergate.grad2019.javafnds.jee.ce3.standards.Command;
import psybergate.grad2019.javafnds.jee.ce3.standards.CommandRequest;
import psybergate.grad2019.javafnds.jee.ce3.standards.CommandResponse;

public class MyCommand implements Command {

	private CommandRequest request;
	
	public MyCommand(CommandRequest commandRequest) {
		request = commandRequest;
	}

	@Override
	public CommandResponse execute() {
		
		return new MyCommandResponse(request.processRequest());
	}
}
