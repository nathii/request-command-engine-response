package psybergate.grad2019.javafnds.jee.ce3.client;

import java.util.ArrayList;
import java.util.Arrays;

import psybergate.grad2019.javafnds.jee.ce3.standards.Command;
import psybergate.grad2019.javafnds.jee.ce3.standards.CommandRequest;
import psybergate.grad2019.javafnds.jee.ce3.standards.CommandResponse;
import psybergate.grad2019.javafnds.jee.ce3.vendorImpl1.AddRequest;
import psybergate.grad2019.javafnds.jee.ce3.vendorImpl1.DateRequest;
import psybergate.grad2019.javafnds.jee.ce3.vendorImpl1.FactorialRequest;
import psybergate.grad2019.javafnds.jee.ce3.vendorImpl1.MyCommand;

public class Initiator {

	public static void main(String[] args) {
		
		CommandRequest commandRequest = requestBuilder(args);
		Command command = new MyCommand(commandRequest);
		CommandResponse response = command.execute();
		System.out.println("Response: " + response.getResponseData());
	}
	
	/**
	 * Takes in args[] and maps the instruction to the correct 
	 * operation/command to be performed.
	 * 
	 * @param requestString instruction from client
	 * @return
	 */
	public static CommandRequest requestBuilder(String[] requestString) {
		
		System.out.println("\nRequest: " + Arrays.toString(requestString));
		String operation = requestString[0].toUpperCase();
		CommandRequest request = null;
		
		switch(operation) {
			
			case "ADD":{
			
				ArrayList<Integer> operands = new ArrayList<>();
				for(int D = 1; D < requestString.length; D++) {
					operands.add(Integer.parseInt(requestString[D]));
				}
				
				request = new AddRequest(operands);
				break;
			}
			
			case "FACT":{
				
				int facto = Integer.parseInt(requestString[1]);
				request = new FactorialRequest(facto);
				break;
			}
			
			case "DATE":{
				
				request = new DateRequest();
				break;
			}
		}
		
		return request;
	}
}
