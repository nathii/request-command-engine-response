package demo.interfaces.dates.standard;

/**
 * Provides a standard interface for a Date. Note, any implementation of Date must be immutable. And, a
 * <code>DateFactory</code> can be used to create a <code>Date</code>
 * 
 * @since 29 Aug 2010
 * @see DateFactory
 */
public interface Date {

  int getYear();

  int getMonth();

  int getDay();

  /**
   * Returns true if current date is a leap year.
   */
  boolean isLeapYear();

  /**
   * Adds numDays to current Date.
   * 
   * @return a new Date. (Date is immutable)
   */
  Date addDays(int numDays);

}
