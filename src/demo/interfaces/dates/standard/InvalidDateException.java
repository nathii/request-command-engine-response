package demo.interfaces.dates.standard;

/**
 * 
 * @since 24 May 2010
 */
@SuppressWarnings("serial")
public class InvalidDateException extends Exception {

  public static final int DAY_INVALID = 1;

  public static final int MONTH_INVALID = 2;

  public static final int YEAR_INVALID = 3;

  private int dateErrorCode;

  public InvalidDateException(int dateErrorCode, String message) {
    super(message);
    this.dateErrorCode = dateErrorCode;
  }

  public int getDateErrorCode() {
    return dateErrorCode;
  }

}
