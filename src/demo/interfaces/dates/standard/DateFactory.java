package demo.interfaces.dates.standard;

/**
 * A <code>DateFactory</code> is a factory that produces <code>Date</code>'s
 * 
 * @since 09 Apr 2015
 * @see demo.interfaces.dates.standard.Date
 */
public interface DateFactory {

  /**
   * Creates a new <code>Date</code>
   * @throws InvalidDateException if year/month/day are not valid for a date.
   */
  public Date createDate(int year, int month, int day) throws InvalidDateException;

}
