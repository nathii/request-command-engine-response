# Execution
---

```
$ cd /To/Directory/Containing/The/Makefile
$ make                           # Compile
$ ARGS="add 1 2 5 7" make run    # To add the numbers 1, 2, 5, 7
$ ARGS="fact 7" make run         # To compute 7! 
$ ARGS="date" make run           # To get the current date
```
