make:
	javac -sourcepath src src/psybergate/grad2019/javafnds/jee/ce3/client/Initiator.java -d target

run:
	java -cp target psybergate.grad2019.javafnds.jee.ce3.client.Initiator $$ARGS

clean: