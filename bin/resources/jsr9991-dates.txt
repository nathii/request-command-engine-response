JSR-9991 (Mock JSR for Implementing the Date Interface)
=======================================================

All valid Date implementations must implement the demo.interfaces.dates.Date interface.
An implementation of Date must be immutable.

The rules for constructing a Date is as follows:
1. Use the DateFactory for creating a Date - if an invalid Date is requested, and InvalidDateException
will be propagated.

The following rules define a VALID date:

1. year can be any valid integer. Negative years represent years before Christ was born e.g. -50 would be 50 BC.
2. Month must be >=1 and <=12
	 1 represents January, 2 represents February, etc
3. As a general rule, day must be >= 1 and <=31. The maximum day for each month is as follows:
	 January 31, February 28 (unless leap year, see below), Mar 31, Apr 30, May 31, Jun 30, Jul 31, Aug 31, 
	 Sep 30, Oct 31, Nov 30, Dec 31
4. If the date represents a leap year, then February will have 29 days. 
5. A leap year is defined as follows:
	 a. For a non-century year, if the year is divisible by 4, then it is a leap year - thus, 2004 is a leap year.
	 b. For a century year, if a year is divisible by 400, then it is a leap year - thus, 2000 is a leap year, 
	 		but 2100 is not.
	 		
	 		
NOTE : This is not a comprehensive doc, but is intended to give you a sense why a JSR more than just a bunch
of interfaces/classes.	 